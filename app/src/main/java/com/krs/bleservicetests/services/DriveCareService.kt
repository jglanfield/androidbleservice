package com.krs.bleservicetests.services

import android.app.*
import android.bluetooth.BluetoothDevice
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import android.util.Log
import com.krs.bleservicetests.ble.BLEListener
import com.krs.bleservicetests.ble.BLEManager
import com.krs.bleservicetests.extensions.getServiceNotification
import com.krs.bleservicetests.R

/**
 * Created by joel on 2018-02-23.
 *
 * NOTE: see the migration guide at: https://developer.android.com/about/versions/oreo/background.html
 * We can:
 * 1. Bind an activity to a service to keep it running, and when the activity stops, start the service in the foreground.
 * 2. Start a foreground service and immediately create a notification. This will keep it in the foreground.
 * 3. Use a JobScheduler to keep it in the background, although this would give us less control.
 */

class DriveCareService : Service(), BLEListener {
    companion object {
        private val TAG = "DriveCareService"
        private val NOTIFICATION_ID = 42
        val CHANNEL_ID = "123456"
    }

    private var notificationManager: NotificationManager? = null

    override fun onCreate() {
        Log.d(TAG, "onCreate")

        BLEManager.listener = this

        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(CHANNEL_ID, getString(R.string.app_name), NotificationManager.IMPORTANCE_LOW)
            notificationManager?.createNotificationChannel(channel)

            startForeground(NOTIFICATION_ID, getServiceNotification("Scanning for devices...", ""))
        }

        // start scanning for beacons
        BLEManager.scan(true)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.d(TAG, "onStartCommand")

        return START_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()

        Log.d(TAG, "in onDestroy()")
    }

    override fun onBind(intent: Intent): IBinder? {
        Log.d(TAG, "onBind()")

        return null
    }

    override fun didStartScanning() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationManager?.notify(NOTIFICATION_ID, getServiceNotification("Scanning for devices...", ""))
        }
    }

    override fun didStopScanning() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationManager?.notify(NOTIFICATION_ID, getServiceNotification("Not currently scanning", ""))
        }
    }

    override fun didFindDevice(device: BluetoothDevice) {
        notificationManager?.notify(NOTIFICATION_ID, getServiceNotification("Found DriveCare Device", device.name))
    }
}