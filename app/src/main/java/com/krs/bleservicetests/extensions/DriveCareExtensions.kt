package com.krs.bleservicetests.extensions

import android.app.Notification
import android.os.Build
import android.support.v4.app.NotificationCompat
import com.krs.bleservicetests.services.DriveCareService
import com.krs.bleservicetests.services.DriveCareService.Companion.CHANNEL_ID
import com.krs.bleservicetests.R

/**
 * Created by joel on 2018-02-26.
 */

fun DriveCareService.getServiceNotification(notificationTitle: String, notificationText: String): Notification {
    val builder = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle(notificationTitle)
            .setContentText(notificationText)
            .setSmallIcon(R.drawable.ic_notice)
            .setWhen(System.currentTimeMillis())

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        builder.setOngoing(true)
    }

    return builder.build()
}

