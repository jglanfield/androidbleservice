package com.krs.bleservicetests

import android.app.Application
import android.content.Intent
import android.os.Build
import android.util.Log
import com.krs.bleservicetests.ble.BLEManager
import com.krs.bleservicetests.services.DriveCareService

/**
 * Created by joel on 2018-02-26.
 */

class DriveCareApplication : Application() {
    companion object {
        val TAG = "DriveCareApplication"
    }

    override fun onCreate() {
        super.onCreate()

        Log.d(TAG, "DriveCareApplication created")

        // start foreground service if API >= Oreo
        val scanningService = Intent(this, DriveCareService::class.java)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(scanningService)
        } else {
            startService(scanningService)
        }
    }

    override fun onTerminate() {
        super.onTerminate()

        Log.d(TAG, "DriveCareApplication terminated")
    }
}