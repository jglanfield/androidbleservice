package com.krs.bleservicetests.ble

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.le.BluetoothLeScanner
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.util.Log
import android.os.Handler


/**
 * Created by joel on 2018-02-23.
 */

interface BLEListener {
    fun didStartScanning()
    fun didStopScanning()
    fun didFindDevice(device: BluetoothDevice)
}

object BLEManager {
    private val TAG = "BLEManager"
    private val SCAN_DURATION_MS: Long = 5000
    private val SCAN_INTERVAL_MS: Long = 5000

    private val btAdapter: BluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
    private val scanner: BluetoothLeScanner
    private var driveCareDevice: BluetoothDevice? = null

    private val handler = Handler()
    private var isScanning = false

    var listener: BLEListener? = null

    init {
        scanner = btAdapter.bluetoothLeScanner
    }

    fun isBluetoothEnabled() : Boolean {
        return btAdapter.isEnabled
    }

    fun scan(enable: Boolean) {
        if (enable && !isBluetoothEnabled()) {
            Log.d(TAG, "Bluetooth adapter disabled. Will retry later.")
            handler.postDelayed({ scan(true) }, 5000L)
            return
        }

        if (!enable) {
            stopScanning()
            handler.postDelayed({ scan(true) }, SCAN_INTERVAL_MS)
        } else if (!isScanning) {
            startScanning()
            handler.postDelayed({ scan(false) }, SCAN_DURATION_MS)
        }
    }

    private fun startScanning() {
        Log.d(TAG, "Start scanning")

        isScanning = true

        scanner.startScan(scanCallBack)
        listener?.didStartScanning()
    }

    private fun stopScanning() {
        Log.d(TAG, "Stop scanning")

        isScanning = false

        if (isBluetoothEnabled()) {
            scanner.stopScan(scanCallBack)
            listener?.didStopScanning()
        }

        driveCareDevice = null
    }

    private val scanCallBack = object : ScanCallback() {
        override fun onScanResult(callbackType: Int, result: ScanResult) {
            val device = result.device
            if (device == driveCareDevice) {
                return
            }

            if (device.name != null) {
                Log.d(TAG, device.name)
                if (device.name.toLowerCase().startsWith("dc-")) {
                    Log.d(TAG, "Found DriveCare device: " + device.name)
                    driveCareDevice = device
                    listener?.didFindDevice(device)
                }
            }
        }

        override fun onBatchScanResults(results: MutableList<ScanResult>) {
            results.forEach { if (it.device.name != null) Log.d(TAG, it.device.name) }
        }

        override fun onScanFailed(errorCode: Int) {
            Log.d(TAG, "Scan failed with error: " + errorCode)
        }
    }



}